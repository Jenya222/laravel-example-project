<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|-----------------
|   Web-Site
|-----------------
*/
Route::get('/', 'PagesController@getHome');
Route::get('/contact', 'PagesController@getContact');
Route::get('/about', 'PagesController@getAbout');
Route::get('/messages', 'MessagesController@getMessages');
Route::post('/contact/submit', 'MessagesController@submit');
/*
|-----------------
|   To-Do list
|-----------------
*/
Route::resource('todo', 'TodosController');
/*
|-----------------
|   Authentication
|-----------------
*/
Auth::routes();
/*
|-----------------
|   Companies listing
|-----------------
*/
Route::resource('listings', 'ListingsController');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
/*
|-----------------
|   PhotoGallery
|-----------------
*/
Route::get('/photoshow/albums', 'AlbumsController@index')->name('photoshow.index');
Route::get('/photoshow/albums/create', 'AlbumsController@create')->name('photoshow.create');
Route::get('/photoshow/albums/{id}', 'AlbumsController@show')->name('photoshow.show');
Route::post('/photoshow/albums/store', 'AlbumsController@store')->name('photoshow.store');
Route::get('/photoshow/photos/create/{id}', 'PhotosController@create');
Route::post('/photoshow/photos/store', 'PhotosController@store');
Route::get('/photoshow/photos/{id}', 'PhotosController@show');
Route::delete('/photoshow/photos/{id}', 'PhotosController@destroy');
/*
|-----------------
|   Api items
|-----------------
*/
Route::resource('api/items', 'ItemsController');