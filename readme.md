## Laravel example project

- Basic Laravel Website.
- Todo List.
- Business Listings.
- Photo Gallery.
- Item Manager REST API.
- ContactStore With Vue.js.

## To run the project
- in folder laravel-env run a command `docker-compose up -d`
- execute `docker exec -it laravel bash`
- execute `mv .env.example .env`
- open the file .env in a text editor and replace
`
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
`
by 

`
 DB_CONNECTION=mysql
 DB_HOST=laravel-mysql
 DB_PORT=3306
 DB_DATABASE=laravel
 DB_USERNAME=root
 DB_PASSWORD=Qq123456789
 `
- execute `php artisan key:generate`
- execute `composer install`
- then execute `php artisan migrate`
- use `control + D` to exit from this connection

After you should be able to open the project by address in browser `http://localhost:8000`


