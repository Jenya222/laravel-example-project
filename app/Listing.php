<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Listing
 *
 * @package App
 */
class Listing extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
