<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Photo
 *
 * @package App
 */
class Photo extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'album_id',
        'description',
        'photo',
        'title',
        'size'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function album()
    {
        return $this->belongsTo('App\Album');
    }
}
