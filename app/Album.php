<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Album
 *
 * @package App
 */
class Album extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'cover_image'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany('App\Photo');
    }
}
