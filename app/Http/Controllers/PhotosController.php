<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class PhotosController
 *
 * @package App\Http\Controllers
 */
class PhotosController extends Controller
{
    /**
     * @param $album_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($album_id)
    {
        return view('photoshow.photos.create')->with('album_id', $album_id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'photo' => 'image|max:1999'
        ]);

        $filenameWithExt = $request->file('photo')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('photo')->getClientOriginalExtension();
        $filenameToStore = $filename . '_' . time() . '.' . $extension;
        $request->file('photo')->storeAs('public/photos/' . $request->input('album_id'), $filenameToStore);

        $photo = new Photo();
        $photo->album_id = $request->input('album_id');
        $photo->title = $request->input('title');
        $photo->description = $request->input('description');
        $photo->size = $request->file('photo')->getSize();
        $photo->photo = $filenameToStore;

        $photo->save();

        return redirect()->route('photoshow.show', ['id' => $request->input('album_id')])->with('success', 'Photo Uploaded');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $photo = Photo::find($id);

        return view('photoshow.photos.show')->with('photo', $photo);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $photo = Photo::find($id);
        $albumId = $photo->album_id;

        if (Storage::delete('public/photos/' . $photo->album_id . '/' . $photo->photo)) {
            $photo->delete();
        }

        return redirect()->route('photoshow.show', ['id' => $albumId])->with('success', 'Photo Deleted');
    }
}
