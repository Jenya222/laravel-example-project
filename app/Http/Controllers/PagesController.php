<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class PagesController
 *
 * @package Controllers
 */
class PagesController extends Controller
{
    /**
     * Render home page
     *
     * @return View
     */
    public function getHome()
    {
        return view('home');
    }

    /**
     * Render about page
     *
     * @return View
     */
    public function getAbout()
    {
        return view('about');
    }

    /**
     * Render contact page
     *
     * @return View
     */
    public function getContact()
    {
        return view('contact');
    }
}
