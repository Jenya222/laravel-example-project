<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class MessagesController
 * 
 * @package App\Http\Controllers
 */
class MessagesController extends Controller
{
    /**
     * Submit contact form
     *
     * @param Request $request
     * @return array|string|null
     * @throws ValidationException
     */
    public function submit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required'
        ]);

        $message = new Message();
        $message->name = $request->input('name');
        $message->email = $request->input('email');
        $message->message = $request->input('message');

        $message->save();

        return redirect('/')->with('success', 'Message Sent');
    }

    /**
     * Get messages
     *
     * @return View
     */
    public function getMessages()
    {
        $messages = Message::all();

        return view('messages')->with('messages', $messages);
    }
}