@extends('layouts.app')

@section('head')
    <meta name="csrf-token" content="{{csrf_token()}}">
@endsection

@section('content')
    <h1>Home</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet consequatur ea esse ex explicabo ipsa ipsam iste maiores mollitia obcaecati omnis provident quas quo ratione totam ut vitae, voluptatem?</p>
    <div id="app">
        <Contacts></Contacts>
    </div>
@endsection

@section('sidebar')
    @parent
    <p>This is appended to the sidebar</p>
@endsection

