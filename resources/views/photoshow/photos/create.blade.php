@extends('layouts.app')

@section('content')
    <h1>Upload Photo</h1>
    {!! Form::open(['action' => 'PhotosController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        {{ Form::bsText('title', '', ['placeholder' => 'Photo title']) }}
        {{ Form::bsTextArea('description', '', ['placeholder' => 'Photo Description']) }}
        {{ Form::hidden('album_id', $album_id) }}
        {{ Form::file('photo') }}
        {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary']) }}
    {!! Form::close() !!}
@endsection