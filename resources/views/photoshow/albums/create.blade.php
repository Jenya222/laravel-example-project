@extends('layouts.app')

@section('content')
    <h1>Create Album</h1>
    {!! Form::open(['action' => 'AlbumsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        {{ Form::bsText('name', '', ['placeholder' => 'Album Name']) }}
        {{ Form::bsTextArea('description', '', ['placeholder' => 'Album Description']) }}
        {{ Form::file('cover_image') }}
        {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary']) }}
    {!! Form::close() !!}
@endsection