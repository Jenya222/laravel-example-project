@extends('layouts.app')

@section('content')
    <h1>{{$album->name}}</h1>
    <a href="/photoshow/albums" class="btn btn-secondary">Go back</a>
    <a href="/photoshow/photos/create/{{$album->id}}" class="btn btn-primary">Upload Photo to Album</a>
    <hr>
    @if(count($album->photos) > 0)
        <?php
        $colcount = count($album->photos);
        $i = 1;
        ?>
        <div id="photos">
            <div class="row text-center">
                @foreach($album->photos as $photo)
                    @if($i == $colcount)
                        <div class='col-lg-4  col-md-4 align-items-end'>
                            <a href="/photoshow/photos/{{$photo->id}}">
                                <img class="thumbnail" src="/storage/photos/{{$photo->album_id}}/{{$photo->photo}}" alt="{{$photo->title}}">
                            </a>
                            <br>
                            <h4>{{$photo->title}}</h4>
                            @else
                                <div class='col-lg-4 col-md-4'>
                                    <a href="/photoshow/photos/{{$photo->id}}">
                                        <img class="thumbnail" src="/storage/photos/{{$photo->album_id}}/{{$photo->photo}}" alt="{{$photo->title}}">
                                    </a>
                                    <br>
                                    <h4>{{$photo->title}}</h4>
                                    @endif
                                    @if($i % 3 == 0)
                                </div></div><div class="row text-center">
                            @else
                        </div>
                    @endif
                    <?php $i++; ?>
                @endforeach
            </div>
        </div>
    @else
        <p>No Photos To Display</p>
    @endif
@endsection